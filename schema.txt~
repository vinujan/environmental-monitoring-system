create table sensor_info(
	sensor_id	INT NOT NULL AUTO_INCREMENT,
	sensor_type	varchar(5) check (sensor_type in ('water','soil','air') ),
	sensor_accuracy	varchar(15),
	parameter_id	INT,
	update_frequency	numeric(4,2),
	sensor_ipaddress	varchar(30),
	last_update	timestamp,
	sensor_model	varchar(10),
	location_id	INT,
	installed_date		date,
	primary key	(sensor_id),
	foreign key	(parameter_id) references parameter(parameter_id),
	foreign key	(location_id) references location(location_id)
);

create table parameter(
	parameter_id	INT NOT NULL AUTO_INCREMENT,
	parameter_name	varchar(10),
	max_limit	numeric(10,4),
	min_limit	numeric(10,4),
	units		varchar(10),
	primary key	(parameter_id)
);

create table location(
	location_id	INT NOT NULL AUTO_INCREMENT,
	location_name	varchar(20),
	longtitude	numeric(8,6),
	latitude	numeric(8,6),
	primary key	(location_id)
);

create table alarm(
	alarm_id	INT NOT NULL AUTO_INCREMENT,
	sensor_id	INT,
	time 		timestamp,
	alarm_description	varchar(1000),
	primary key	(alarm_id),
	foreign key	(sensor_id) references sensor_info(sensor_id)
);

create table reading(
	sensor_id	INT,
	reading_time	timestamp,
	reading_value	numeric(10,4),	
	primary key (sensor_id,reading_time),
	foreign key 	(sensor_id) references sensor_info(sensor_id)
);

create table maintenance(
	maintenance_id	INT NOT NULL AUTO_INCREMENT,
	sensor_id	INT,
	maintenance_date	date,
	maintenance_description	varchar(1000),
	employee_id	INT,
	primary key (maintenance_id),
	foreign key 	(sensor_id) references sensor_info(sensor_id),
	foreign key 	(employee_id) references maintenance_team(employee_id)
);

create table maintenance_team(
	employee_id	INT NOT NULL AUTO_INCREMENT,
	employee_name	varchar(20),
	primary key 	(employee_id)
);

create table reports(	
	report_id	INT NOT NULL AUTO_INCREMENT,
	report_type	varchar(10),
	time_period	varchar(20),
	report_pointer	mediumblob,
	report_description	varchar(1000),
	primary key 	(report_id)
);

create table employee_contact_number(
	employee_id	INT,
	contact_number	numeric(10,0),
	primary key	(employee_id,contact_number),
	foreign key 	(employee_id) references maintenance_team(employee_id)
);
