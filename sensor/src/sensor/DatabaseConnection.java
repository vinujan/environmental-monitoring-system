package sensor;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DatabaseConnection {
    private PreparedStatement pst=null;
    private Connection con=null;
    private ResultSet rs=null;
    private ArrayList<Sensor> sensors;

    public DatabaseConnection(){
        sensors=new ArrayList<Sensor>();
    }

    public void saveToMem(){
        con = Connect.ConnectDB();
        String sql= "select sensor_id, sensor_type, parameter_name, max_limit, min_limit, units, update_frequency, last_update from sensor_info natural join parameter ORDER BY sensor_id ASC ;";
        try
        {
            pst=con.prepareStatement(sql);
            rs= pst.executeQuery();
            while (rs.next()) {
                Sensor sen=new Sensor();
                sen.setSensor_id(rs.getInt("sensor_id"));
                sen.setSensor_type(rs.getString("sensor_type"));
                sen.setParam_name(rs.getString("parameter_name"));
                sen.setMax_lim(rs.getDouble("max_limit"));
                sen.setMin_lim(rs.getDouble("min_limit"));
                sen.setUnit(rs.getString("units"));
                sen.setUpdate_time(rs.getInt("update_frequency"));
                sen.setLast_update(rs.getTimestamp("last_update"));
                sensors.add(sen);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getLength(){
        return sensors.size();
    }

    public Sensor getSensor(int position){
        return sensors.get(position);
    }

    public String[] getSensorIdList(){
        int len=sensors.size();
        String sensorIdList[]=new String[len];
        for (int i=0;i<len; i++){
            sensorIdList[i]= String.valueOf(sensors.get(i).getSensor_id());
        }
        return sensorIdList;
    }
}
