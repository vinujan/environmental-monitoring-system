package sensor;


import javax.swing.*;
import java.util.Date;
import java.util.Random;

public class Sensor implements Runnable{
    private int sensor_id;
    private String sensor_type;
    private String param_name;
    private double max_lim;
    private double min_lim;
    private String unit;
    private int update_time;
    private Date last_update;
    private double reading=-1;
    private ServerConnection server;
    private JLabel infoLabel;


    public int getSensor_id() {
        return sensor_id;
    }

    public void setSensor_id(int sensor_id) {
        this.sensor_id = sensor_id;
    }

    public String getSensor_type() {
        return sensor_type;
    }

    public void setSensor_type(String sensor_type) {
        this.sensor_type = sensor_type;
    }

    public String getParam_name() {
        return param_name;
    }

    public void setParam_name(String param_name) {
        this.param_name = param_name;
    }

    public double getMax_lim() {
        return max_lim;
    }

    public void setMax_lim(double max_lim) {
        this.max_lim = max_lim;
    }

    public double getMin_lim() {
        return min_lim;
    }

    public void setMin_lim(double min_lim) {
        this.min_lim = min_lim;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(int update_time) {
        this.update_time = update_time;
    }

    public double getRandomReading(long seed){
        Random random = new Random(seed);
        if (min_lim > max_lim) {
            throw new IllegalArgumentException("Start cannot exceed End.");
        }
        //get the range, casting to long to avoid overflow problems
        double range = max_lim - min_lim ;
        // compute a fraction of the range, 0 <= frac < range
        double fraction = (range * random.nextDouble());
        return fraction + min_lim;
    }

    public Date getLast_update() {
        return last_update;
    }

    public void setLast_update(Date last_update) {
        this.last_update = last_update;
    }

    public void setReading(double reading) {
        this.reading = reading;
    }

    @Override
    public void run() {
        if (reading==-1){
            reading= getRandomReading(System.nanoTime());
        }
        server.sendData(String.valueOf(getSensor_id()),reading,this );
    }

    public void setServerConnection(ServerConnection serverConnection) {
        this.server = serverConnection;
    }
}