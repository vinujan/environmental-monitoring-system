package sensor;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.concurrent.*;
import java.util.concurrent.atomic.DoubleAccumulator;


public class SensorGUI extends JFrame {
    private static DatabaseConnection data;
    private static ScheduledExecutorService[] sensorSend;
    private final JTextField readingTXT;
    private final JButton btnSend;
    private Sensor sensor;
    private double reading;
    private ServerConnection server =new ServerConnection();

    public SensorGUI () {

        configureScheduledTasks();

        Container cp = getContentPane();
        cp.setLayout(new GridLayout(5,3));
        JCheckBox autoCheckBox=new JCheckBox("Manual");
        autoCheckBox.setSelected(false);
        autoCheckBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    readingTXT.setEditable(true);
                    btnSend.setEnabled(true);
                } else {
                    readingTXT.setEditable(false);
                    btnSend.setEnabled(false);
                    sensor.setReading(-1);
                }
            }
        });

        cp.add(autoCheckBox);
        cp.add(new JLabel());
        cp.add(new JLabel("Sensor ID"));
        final JComboBox sensorList = new JComboBox(data.getSensorIdList());
        readingTXT = new JTextField(10);
        sensor =data.getSensor(0);
        sensorList.setSelectedIndex(0);
        sensorList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int sensorId=sensorList.getSelectedIndex();
                sensor =data.getSensor(sensorId);
            }
        });
        cp.add(sensorList);
        cp.add(new JLabel("Reading"));;
        cp.add(readingTXT);
        cp.add(new JLabel());
        btnSend = new JButton("Send Now");
        cp.add(btnSend);

        cp.add(new JLabel("last send"));
        final JLabel info = new JLabel();
        server.setInfoLabel(info);
        cp.add(info);




        btnSend.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    reading= Double.parseDouble(readingTXT.getText());
                    sensor.setReading(reading);
                    sensor.run();
                }
                catch (NumberFormatException cat){
                    if (readingTXT.getText().equals("")){
                        sensor.setReading(-1);
                        sensor.run();
                    }
                    else
                        JOptionPane.showMessageDialog(null, "Reading value is not legal !!!");
                }
            }
        });

        readingTXT.setEditable(false);
        btnSend.setEnabled(false);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("Sensor");
        setSize(400, 200);
        setVisible(true);
    }

    public static void main(String[] args) {
        data=new DatabaseConnection();
        data.saveToMem();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new SensorGUI();
            }
        });
    }

    private void configureScheduledTasks(){
        int len=data.getLength();
        System.out.println(len);
        sensorSend=new ScheduledExecutorService[len];
        for (int i=0;i<len;i++) {
            Sensor sen=data.getSensor(i);
            sen.setServerConnection(server);
            sensorSend[i] = Executors.newSingleThreadScheduledExecutor();
            sensorSend[i].scheduleWithFixedDelay(sen, i/60, sen.getUpdate_time(), TimeUnit.MINUTES);
        }
    }
}