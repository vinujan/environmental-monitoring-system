package sensor;

import org.json.JSONException;
import org.json.JSONObject;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;


public class ServerConnection {

    private JLabel infoLabel;

    public void sendData(String sensorId, double reading, Sensor sensor) {
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL("http://127.0.0.1:8000/input/reading");
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setUseCaches(false);
            urlConnection.setConnectTimeout(120000);
            urlConnection.setReadTimeout(120000);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.connect();

            JSONObject jsonParam = new JSONObject();
            jsonParam.put("sensor_id", sensorId);
            jsonParam.put("readingValue", reading);

            //1453226194113
            java.util.Date date= new java.util.Date();
            jsonParam.put("readingTime", new Timestamp(date.getTime()));
            OutputStreamWriter out = new OutputStreamWriter(urlConnection.getOutputStream());
            out.write(jsonParam.toString());
            out.close();


            int HttpResult = urlConnection.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        urlConnection.getInputStream(), "utf-8"));
                infoLabel.setText("Sensor id : " + sensorId + "  Reading :" + reading);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }

    }

    public void setInfoLabel(JLabel info) {
        infoLabel=info;
    }
}
