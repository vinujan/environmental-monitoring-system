<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 18/01/16
 * Time: 16:32
 */

namespace AppBundle\Controller;
use AppBundle\Entity\Alarm;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DatasheetController extends Controller
{
    /**
     * @Route("/admin/input/datasheet", name="datasheet")
     */
    public function displayDatasheet(Request $request)
    {
        require_once('../src/AppBundle/Service/AlarmService.php');
        require_once('../src/AppBundle/Service/LocationService.php');
        require_once('../src/AppBundle/Service/ParameterService.php');
        $alarmCount = getAlarmCountDB();
        $locationList=array_flip(getLocationList());
        $parameterList=array_flip(getParameterList());
        return $this->render(
            'datasheet.html.twig',
            array("alarm_count"=>$alarmCount,"parameter"=>$parameterList,"location"=>$locationList
            )
        );
    }

    /**
     * @Route("/admin/show/datasheet", name="datasheetProcessed")
     * @Method("Post")
     */
    public function processAndDisplayDatasheet(Request $request){
        require_once('../src/AppBundle/Service/ReadingService.php');
        $datasheetRequest = $request->getContent();
        $params = json_decode($datasheetRequest,true);//data will be come like this ["",""]
        $parameterList = $this->multiexplode($params['parameter']);
        $locationList = $this->multiexplode($params['location']);
        $isDate = $params['isDate'];
        $dateRangeList = $this->multiexplode($params['dateRange']);//start range , end range
        $query = $this->generateQuery($parameterList,$locationList,$isDate,$dateRangeList);
        $response = array();
        $response['tableHeader']= array("sensor_id","parameter_name","location_name","reading_time","reading_value");
        $response['tableData'] = forDataSheet($query);
        return new JsonResponse($response);
    }

    public function multiexplode ($string) {

        $ready = implode(explode("]",implode(explode("[",$string))));
        return  explode(",",$ready);
    }

    public function generateQuery($parameterList,$locationList,$isDate,$dateRange){
        if($parameterList[0] == '"all"'){
            require_once('../src/AppBundle/Service/ParameterService.php');
            $parameterList=array_values(array_flip(getParameterList()));
        }
        if($locationList[0] === '"all"'){
            require_once('../src/AppBundle/Service/LocationService.php');
            $locationList=array_values(array_flip(getLocationList()));
        }
        $query = "select sensor_id,parameter_name,location_name,reading_time,reading_value from sensor_info natural join reading natural join parameter natural join location where";
        $parameterQuery = "";
        for($i = 0;$i < count($parameterList) ;$i++){
            if($i === 0){
                $parameterQuery = $parameterQuery." ( parameter_name ='".str_replace('"', "",$parameterList[$i])."'";
            }else{
                $parameterQuery = $parameterQuery." OR parameter_name ='".str_replace('"', "",$parameterList[$i])."'";
            }
        }
        $parameterQuery = $parameterQuery." )";
        $locationQuery = "";
        for($i = 0;$i < count($locationList) ;$i++){
            if($i === 0){
                $locationQuery = $locationQuery." ( location_name ='".str_replace('"', "",$locationList[$i])."'";
            }else{
                $locationQuery = $locationQuery." OR location_name ='".str_replace('"', "",$locationList[$i])."'";
            }
        }
        $locationQuery = $locationQuery." )";
        $query = $query.$parameterQuery." AND ".$locationQuery;
        if($isDate !== "no" && $dateRange[0] !== '""' && $dateRange[1] !== '""'){
            $query = $query." AND (reading_time BETWEEN '".str_replace('"', "", $dateRange[0])."' AND '".str_replace('"', "", $dateRange[1])."')";
        }
        return $query.";";
    }

    /**
     * @Route("/admin/pdf", name="createPdf")
     */
    public function createPdf(Request $request)
    {
        require_once('../src/AppBundle/Service/AlarmService.php');
        require_once('../src/AppBundle/Service/LocationService.php');
        require_once('../src/AppBundle/Service/ParameterService.php');
        $alarmCount = getAlarmCountDB();
        $locationList=array_flip(getLocationList());
        $parameterList=array_flip(getParameterList());
        $html = $this->renderView('datasheet.html.twig',
            array("alarm_count"=>$alarmCount,"parameter"=>$parameterList,"location"=>$locationList
        ));

        return new Response(
            $this->get('test.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="file.pdf"'
            )
        );
    }
}