<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 10/12/15
 * Time: 22:12
 */
namespace AppBundle\Controller;

use AppBundle\Entity\Alarm;
use AppBundle\Entity\DummyUser;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class EMSController extends Controller
{
    /**
     * @Route("/", name="login_route")
     */
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render(
            'security/login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error,
            )
        );
    }


    /**
     * @Route("/login_check", name="login_check")
     */
    public function loginCheckAction()
    {
        // this controller will not be executed,
        // as the route is handled by the Security system
    }

    /**
     * @Route("/logging_in")
     */
    public function loggingIn(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        switch($user->getRoles()[0]) {
            case "ROLE_ADMIN":
                return $this->redirectToRoute("adminHome");
                break;
            case "ROLE_MAINT":
                return $this->redirectToRoute("maintainHome");
                break;
            default:
                return $this->redirectToRoute("login_route");
        }
    }




    /**
     * @Route("admin/adminHome",name="adminHome")
     */
    public function  showAdminHomePage(){
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();
        return $this->render('adminHome.html.twig',array("alarm_count"=>$alarmCount));
    }

    /**
     * @Route("maint/maintainHome",name="maintainHome")
     */
    public function  showMaintainHomePage(){
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();
        return $this->render('maintainanceHome.html.twig',array("alarm_count"=>$alarmCount));
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout(Request $request)
    {
        //log out
    }
    /**
     * @Route("/input/reading")
     * @Method("POST")
     */
    public function createReading(Request $request)
    {
        require_once('../src/AppBundle/Service/ReadingService.php');
        require_once('../src/AppBundle/Service/AlarmService.php');
        require_once('../src/AppBundle/Service/ParameterService.php');
        require_once('../src/AppBundle/Service/SensorInfoService.php');
        $readingRequest = $request->getContent();
        $json = new Serializer(array(new ObjectNormalizer()), array(new XmlEncoder(), new JsonEncoder()));
        $reading = $json->deserialize($readingRequest, 'AppBundle\Entity\Reading', 'json');
        createReadingDB($reading);
        updateLastUpdate($reading);         //this call update the last update field in the sensor info
        $isAlarm = checkReadingDB($reading);// this function will return 1 if there is a alarm else it will return 0
        $limit = getParameterLimit($reading);
        if($isAlarm == 1){
            $alarm = new Alarm();
            $alarm->setSensorId($reading->getSensorId());
            $alarm->setAlarmDescription("Reading value is ".$reading->getReadingValue()." is not with in the range ".$limit[0]." to ".$limit[1]);
            createAlarmDB($alarm);
        }
        return new Response("true");
    }

    /**
     * @Route("admin/changePassword", name="changeAdminPassword")
     */
    public function changeAdminPassword(Request $request){

        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();


        $u = $this->get('security.token_storage')->getToken()->getUser();

        $user=new DummyUser();
        $user->setUserName($u->getUsername());

        $form=$this->createFormBuilder($user)
            ->add('userName', TextType::class,array('attr'=>array('maxLength'=>10),'disabled' => true))
            ->add('newPassword',PasswordType::class)
            ->add('conformPassword',PasswordType::class)
            ->add('save', SubmitType::class, array('label' => 'change password'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()&& $form->isValid()) {
            $newPass=$user->getNewPassword();
            $conPass=$user->getConformPassword();
            if ($newPass!=$conPass){
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'conformation password not matched'
                );
            }
            else{
             $password = $this->get('security.password_encoder')
                 ->encodePassword(new User(), $user->getNewPassword());
                $user->setNewPassword($password);
                require_once('../src/AppBundle/Service/UserService.php');
                changePasswordDB($user);
                return $this->redirectToRoute("adminHome");
            }
        }


        return $this->render('adminEdit.html.twig', array(
            'form' => $form->createView(),"alarm_count"=>$alarmCount,"formName"=>"Password"
        ));
    }

    /**
     * @Route("/maint/changePassword", name="changeMaintPassword")
     */
    public function changeMaintPassword(Request $request){

        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();


        $u = $this->get('security.token_storage')->getToken()->getUser();

        $user=new DummyUser();
        $user->setUserName($u->getUsername());

        $form=$this->createFormBuilder($user)
            ->add('userName', TextType::class,array('attr'=>array('maxLength'=>10),'disabled' => true))
            ->add('newPassword',PasswordType::class)
            ->add('conformPassword',PasswordType::class)
            ->add('save', SubmitType::class, array('label' => 'change password'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()&& $form->isValid()) {
            $newPass=$user->getNewPassword();
            $conPass=$user->getConformPassword();
            if ($newPass!=$conPass){
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'conformation password not matched'
                );
            }
            else{
                $password = $this->get('security.password_encoder')
                    ->encodePassword(new User(), $user->getNewPassword());
                $user->setNewPassword($password);
                require_once('../src/AppBundle/Service/UserService.php');
                changePasswordDB($user);

                return $this->redirectToRoute("maintainHome");
            }
        }


        return $this->render('maintenanceReport.html.twig', array(
            'form' => $form->createView(),"alarm_count"=>$alarmCount,"formName"=>"Password"
        ));
    }
}