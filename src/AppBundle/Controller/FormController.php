<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 01/01/16
 * Time: 23:07
 */

namespace AppBundle\Controller;

use AppBundle\Entity\DummyUser;
use AppBundle\Entity\Location;
use AppBundle\Entity\Maintenance;
use AppBundle\Entity\Parameter;
use AppBundle\Entity\SensorInfo;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Validator\Constraints\Time;


class FormController extends Controller
{
    /**
     * @Route("maint/maintainReport/{id}",name="maintainReport")
     */
    public function  createMaintainanceReport(Request $request,$id){

        $uId = $this->get('security.token_storage')->getToken()->getUser()->getId();

        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();

        $maintenance = new Maintenance();
        $maintenance->setMaintenanceDate(new \DateTime('today'));

        require_once('../src/AppBundle/Service/AlarmService.php');
        require_once('../src/AppBundle/Service/MaintenanceService.php');

        $form = $this->createFormBuilder($maintenance)
            ->add('alarm_id',TextType::class, array('data' => $id,
                'disabled' => true))
            ->add('maintenanceDate', DateType::class)
            ->add('maintenanceDescription', TextType::class,array('attr' => array('maxlength' => 1000)))
            ->add('save',SubmitType::class, array('label' => 'save report'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $maintenance->setMaintenanceDate($maintenance->getMaintenanceDate()->format("Y-m-d"));
            $maintenance->setEmployeeId($uId);
            $maintenance->setAlarmId($id);
            $result=createMaintenanceDB($maintenance);
            if ($result[0]==false){
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'maintenance report creation failed !!!'
                );
            }
            else{
                require_once('../src/AppBundle/Service/AlarmService.php');
                updateStatus($id);
                $key = array('alarmId','maintenanceDate','maintenanceDescription','employee_id');
                $value= array($maintenance->getAlarmId(),$maintenance->getMaintenanceDate(),$maintenance->getMaintenanceDescription()
                ,$maintenance->getEmployeeId());
                return $this->render('maintenanceFormDisplay.html.twig',array('formName'=>'Maintenance Report','key'=>$key,'value'=>$value,"alarm_count"=>$alarmCount));
            }

        }

        return $this->render('maintenanceReport.html.twig', array(
            'form' => $form->createView(),'formName'=>'Maintenance Report',"alarm_count"=>$alarmCount
        ));
    }

    /**
     * @Route("/admin/create_user", name="createUser")
     */
    public function createUser(Request $request)
    {
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();

        $duser=new DummyUser();
        $duser->setNewPassword("qwert");

        $form=$this->createFormBuilder($duser)
            ->add('userName', TextType::class,array('attr'=>array('maxLength'=>10)))
            ->add('roles',ChoiceType::class, array(
                'choices'  => array(
                    'admin' => 'ROLE_ADMIN',
                    'maintain' => 'ROLE_MAINT',
                ),
                // *this line is important*
                'choices_as_values' => true,
            ))
            ->add('save', SubmitType::class, array('label' => 'add user'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user=new User();
            $password = $this->get('security.password_encoder')
                ->encodePassword($user, $duser->getNewPassword());
            $user->setPassword($password);
            $user->setUsername($duser->getUserName());
            $user->setRoles($duser->getRoles());

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

//
//            if ($em->contains($user)){
//                $this->get('session')->getFlashBag()->add(
//                    'notice',
//                    'user creation failed !!!   Already a user available with this user name'
//                );
//            }
//            else{
                $key = array('userName','role');
                $value= array($user->getUserName(),$user->getRole());
                return $this->render('adminFormDisplay.html.twig',array('formName'=>'User','key'=>$key,'value'=>$value,"alarm_count"=>$alarmCount));
//            }
        }

        return $this->render('adminEdit.html.twig', array(
            'form' => $form->createView(),"alarm_count"=>$alarmCount,"formName"=>"User"
        ));
    }

    /**
     * @Route("/admin/input/location", name="createLocation")
     *
     */
    public function createLocation(Request $request)
    {
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();

        require_once('../src/AppBundle/Service/LocationService.php');
        $location=new Location();
        $locationId=getLastLocationId()+1;
        $form=$this->createFormBuilder($location)
            ->add('locationId', NumberType::class, array(
                'data' => $locationId,
                'disabled' => true))
            ->add('locationName',TextType::class,array('attr' => array('maxlength' => 20)))
            ->add('longtitude',NumberType::class,array('attr' => array('Locale-specific' => 6)))
            ->add('latitude',NumberType::class,array('attr' => array('Locale-specific' => 6)))
            ->add('save', SubmitType::class, array('label' => 'add location'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $result=createLocationDB($location);
            if ($result[0]==false){
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Location creation failed !!!'
                );
            }
            else{
                $key = array('locationId','locationName','longtitude','latitude');
                $value= array($location->getLocationId(),$location->getLocationName(),$location->getLongtitude(),$location->getLatitude());
                return $this->render('adminFormDisplay.html.twig',array('formName'=>'Location','key'=>$key,'value'=>$value,"alarm_count"=>$alarmCount));
            }
        }

        return $this->render('adminEdit.html.twig', array(
            'form' => $form->createView(),"alarm_count"=>$alarmCount,"formName"=>"Location"
        ));
    }

    /**
     * @Route("/admin/input/parameter", name="createParameter")
     *
     */
    public function createParameter(Request $request)
    {
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();

        require_once('../src/AppBundle/Service/ParameterService.php');
        $parameter=new Parameter();
        $parameterId=getLastParameterId()+1;
        $form=$this->createFormBuilder($parameter)
            ->add('parameterId', NumberType::class, array(
                'data' => $parameterId,
                'disabled' => true))
            ->add('parameterName',TextType::class,array('attr' => array('maxlength' => 10)))
            ->add('MinLimit',NumberType::class)
            ->add('MaxLimit',NumberType::class)
            ->add('Units',TextType::class,array('attr' => array('maxlength' => 20)))
            ->add('save', SubmitType::class, array('label' => 'add parameter'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $result=createParameterDB($parameter);
            if ($result[0]==false){
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Parameter creation failed !!!'
                );
            }
            else{
                $key = array('parameterId','parameterName','MinLimit','MaxLimit','Units');
                $value= array($parameter->getParameterId(),$parameter->getParameterName(),$parameter->getMinLimit(),
                                $parameter->getMaxLimit(),$parameter->getUnits());
                return $this->render('adminFormDisplay.html.twig',array('formName'=>'Parameter','key'=>$key,'value'=>$value,"alarm_count"=>$alarmCount));
            }
        }

        return $this->render('adminEdit.html.twig', array(
            'form' => $form->createView(),"alarm_count"=>$alarmCount,"formName"=>"Parameter"
        ));

    }

    /**
     * @Route("/admin/input/sensor_info", name="createSensor")
     */
    public function createSensorInfo(Request $request)
    {
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();

        require_once('../src/AppBundle/Service/SensorInfoService.php');
        require_once('../src/AppBundle/Service/LocationService.php');
        require_once('../src/AppBundle/Service/ParameterService.php');

        $sensorInfo=new SensorInfo();
        $sensorInfo->setInstalledDate(new \DateTime('today'));
        $sensorInfo->setUpdateFrequency(new \DateTime('00:01'));
        $sensorId=getLastSensorId()+1;
        $locationList=getLocationList();
        $parameterList=getParameterList();
        $form=$this->createFormBuilder($sensorInfo)
            ->add('sensorId', NumberType::class, array(
                'data' => $sensorId,
                'disabled' => true))
            ->add('sensorType',ChoiceType::class, array(
                'choices'  => array(
                    'water' => 'water' ,
                    'soil' => 'soil',
                    'air' => 'air'
                ),
                'choices_as_values' => true))
            ->add('locationId', ChoiceType::class, array(
                'choices'  =>$locationList                                                                                                    ,
                'choices_as_values' => true))
            ->add('parameterId', ChoiceType::class, array(
                'choices'  =>$parameterList                                                                                                    ,
                'choices_as_values' => true))
            ->add('installedDate', DateType::class)
            ->add('sensorModel',TextType::class,array('attr' => array('maxlength' => 10)))
            ->add('sensorIpaddress', TextType::class,array('attr' => array('maxlength' => 15)))
            ->add('sensorAccuracy', NumberType::class, array('attr' => array('maxlength' => 2)))
            ->add('updateFrequency', TimeType::class,  array('label' => 'Update Frequency (hh:mm)'))
            ->add('save', SubmitType::class, array('label' => 'add sensor'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $updateFrequency =  explode(":",$sensorInfo->getUpdateFrequency()->format("H:i"));
            $sensorInfo->setUpdateFrequency((intval($updateFrequency[0])*60+intval($updateFrequency[1])));
            $sensorInfo->setSensorId($sensorId);
            $sensorInfo->setInstalledDate($sensorInfo->getInstalledDate()->format("Y-m-d"));
            $result=createSensorInfoDB($sensorInfo);
            if ($result[0]==false){
                $this->get('session')->getFlashBag()->add(
                    'notice',
                    'Sensor creation failed !!!'
                );
            }
            else{
                $key = array('sensorId','sensorType','location','parameter','installedDate',
                            'sensorModel','sensorIpaddress','sensorAccuracy','updateFrequency');
                $value= array($sensorInfo->getSensorId(),$sensorInfo->getSensorType(),$sensorInfo->getLocationId(),
                            $sensorInfo->getParameterId(),$sensorInfo->getInstalledDate(),$sensorInfo->getSensorModel(),
                            $sensorInfo->getSensorIpaddress(),$sensorInfo->getSensorAccuracy(),$sensorInfo->getUpdateFrequency());
                return $this->render('adminFormDisplay.html.twig',array('formName'=>'SensorInfo','key'=>$key,'value'=>$value,"alarm_count"=>$alarmCount));
            }
        }
        return $this->render('adminEdit.html.twig', array(
            'form' => $form->createView(),"alarm_count"=>$alarmCount,"formName"=>"Sensor"
        ));
    }
}