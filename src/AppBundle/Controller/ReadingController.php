<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 10/01/16
 * Time: 22:55
 */

namespace AppBundle\Controller;


use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Zend\Json\Expr;

class ReadingController extends Controller
{

    /**
     * @Route("/admin/graph/{locationId}/{parameterId}/{year}", name="viewGraphLocation")
     */
    public function viewGraphlLocation($locationId=0,$parameterId=1,$year=2016){
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();

        require_once('../src/AppBundle/Service/ParameterService.php');
        $parameterDetail=getParameterDetailDB($parameterId);

        require_once('../src/AppBundle/Service/LocationService.php');
        $locationDetail=getLocationDetailDB($locationId);

//        if ($parameterDetail==null){
//            return new JsonResponse("wrong parameter");
//        }

//        if ($locationDetail==null){
//            return new JsonResponse("wrong location");
//        }
        $tittle=$parameterDetail[0]['parameter_name'].'   ( '.$parameterDetail[0]['units'].' )';

        require_once('../src/AppBundle/Service/ReadingService.php');
        if ($locationId==0){
            $result=getAvarageReadingMonthly($parameterId,$year);
        }
        else{
            $result=getAvarageReadingOnLocationMonthly($locationId,$parameterId,$year);
        }
        $dataArray = array();
        for ($i=0; $i<count($result); $i++) {
            array_push($dataArray, (float) $result[$i]['avg']);
        }

//        return new JsonResponse($dataArray);

        $series = array(
            array(
                'name'  => $parameterDetail[0]['parameter_name'],
                'type'  => 'column',
                'color' => '#4572A7',
                'yAxis' => 0,
                'data'  => $dataArray,
            ),
        );
        $unit=$parameterDetail[0]['units'];
        $yData = array(
            array(
                'labels' => array(
//                    'formatter' => new Expr('function () { return this.value + " mm" }'),
                    'style'     => array('color' => '#4572A7')
                ),
                'gridLineWidth' => 0,
                'title' => array(
                    'text'  => $tittle,
                    'style' => array('color' => '#4572A7')
                ),
            ),
        );


        $categories = array();
        for ($i=0; $i<count($result); $i++) {
            array_push($categories, (float) $result[$i]['month']);
        }


        $ob = new Highchart();
        $ob->chart->renderTo('linechart'); // The #id of the div where to render the chart
        $ob->chart->type('column');
        if ($locationId==0){
            $ob->title->text('Average Monthly '.$parameterDetail[0]['parameter_name'].' for Colombo city - All locations');
        }
        else{
            $ob->title->text('Average Monthly '.$parameterDetail[0]['parameter_name'].' for Colombo city - '.$locationDetail[0]['location_name']);

        }
        $ob->xAxis->categories($categories);
        $ob->yAxis($yData);
        $ob->legend->enabled(false);
        $formatter = new Expr('function () {
                 var unit = {
                     "humidity": "%",
                 }[this.series.name];
                 return this.x + ": <b>" + this.y + "</b> ";
             }');
        $ob->tooltip->formatter($formatter);
        $ob->series($series);

        require_once('../src/AppBundle/Service/LocationService.php');
        $locations = getLocationNameList();

        require_once('../src/AppBundle/Service/ParameterService.php');
        $paramters = getParameterNameList();

        require_once('../src/AppBundle/Service/ReadingService.php');
        $years = getYearList();

        return $this->render('graph.html.twig', array(
            'chart' => $ob,"alarm_count"=>$alarmCount,"locationId"=>$locationId,"parameterId"=>$parameterId,"year"=>$year,"url"=>"viewGraphLocation",
            "locations"=>$locations,"parameters"=>$paramters, "years"=>$years
        ));
    }
}