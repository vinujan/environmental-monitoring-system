<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 01/01/16
 * Time: 23:35
 */

namespace AppBundle\Controller;

use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Zend\Json\Expr;

class ViewController extends Controller
{

    /**
     * @Route("/admin/display/alarm/{sort}", name="displayAlarm")
     */
    public function diplayAlarm($sort)
    {
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();

        require_once('../src/AppBundle/Service/AlarmService.php');
        $result = getAlarmDetailsDB($sort);

//        require_once('../src/AppBundle/Service/MaintenanceService.php');
//        $result = getMaintenanceDB();

        return $this->render('adminAlarmView.html.twig',array("sortBy"=>$result[0],"tableHeader"=>$result[1],
            "tableData"=>array_slice($result,2),"alarm_count"=>$alarmCount,"tableName"=>"Alarm","url"=>"displayAlarm"));
    }

    /**
     * @Route("/maint/display/alarm/{sort}", name="displayMaintAlarm")
     */
    public function displayMaintAlarm($sort)
    {
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();

        require_once('../src/AppBundle/Service/AlarmService.php');
        $result = getAlarmDetailsDB($sort);


        return $this->render('maintAlarmView.html.twig',array("sortBy"=>$result[0],"tableHeader"=>$result[1],
            "tableData"=>array_slice($result,2),"alarm_count"=>$alarmCount,"tableName"=>"Alarm","url"=>"displayMaintAlarm"));
    }

    /**
     * @Route("/admin/display/solved/{id}", name="displaySolvedAlarm")
     */
    public function displaySolvedAlarm($id){
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();

        require_once('../src/AppBundle/Service/MaintenanceService.php');
        $result = getMaintenanceDetail($id);

        $key = array('alarm Id', 'maintenance Date', 'maintenance Description', 'employee id');
        $value = array($result[0]['alarm_id'], $result[0]['maintenance_date'], $result[0]['maintenance_description'],
            $result[0]['employee_id']);
        return $this->render('adminFormDisplay.html.twig', array('formName' => 'Maintenance Report', 'key' => $key, 'value' => $value, "alarm_count" => $alarmCount));

    }

    /**
     * @Route("/admin/display//location/{sort}", name="displayLocationSortBy")
     * @Route("/admin/display/location", name="displayLocation")
     */
    public function displayLocation($sort="location_id")
    {
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();

        require_once('../src/AppBundle/Service/LocationService.php');
        $result = getLocationDetailsDB($sort);
        return $this->render('adminTableView.html.twig',array("sortBy"=>$result[0],"tableHeader"=>$result[1],
            "tableData"=>array_slice($result,2),"alarm_count"=>$alarmCount,"tableName"=>"Location","url"=>"displayLocationSortBy"));
    }

    /**
     * @Route("/admin/display/parameter/{sort}", name="displayParameterSortBy")
     * @Route("/admin/display/parameter", name="displayParameter")
     */
    public function displayParameter($sort="parameter_id")
    {
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();

        require_once('../src/AppBundle/Service/ParameterService.php');
        $result = getParameterDetailsDB($sort);
        return $this->render('adminTableView.html.twig',array("sortBy"=>$result[0],"tableHeader"=>$result[1],
            "tableData"=>array_slice($result,2),"alarm_count"=>$alarmCount,"tableName"=>"Parameter","url"=>"displayParameterSortBy"));
    }

    /**
     * @Route("/admin/display/sensor/{sort}", name="displaySensorSortBy")
     * @Route("/admin/display/sensor", name="displaySensor")
     */
    public function displaySensor($sort="sensor_id")
    {
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();

        require_once('../src/AppBundle/Service/SensorInfoService.php');
        $result = getSensorDetailsDB($sort);
        return $this->render('adminTableView.html.twig',array("sortBy"=>$result[0],"tableHeader"=>$result[1],
            "tableData"=>array_slice($result,2),"alarm_count"=>$alarmCount,"tableName"=>"Sensor","url"=>"displaySensorSortBy"));
    }

    /**
     * @Route("/admin/display/user/{sort}", name="displayUserSortBy")
     * @Route("/admin/display/user", name="displayUser")
     */
    public function displayUser($sort='id')
    {
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();

        require_once('../src/AppBundle/Service/UserService.php');
        $result = getUserDetailsDB($sort);
        return $this->render('adminTableView.html.twig',array("sortBy"=>$result[0],"tableHeader"=>$result[1],
            "tableData"=>array_slice($result,2),"alarm_count"=>$alarmCount,"tableName"=>"User","url"=>"displayUserSortBy"));
    }


    /**
     * @Route("/admin/delete/{table}/{id}", name="deleteAction")
     */
    public function tableAction($id,$table)
    {
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();

        switch ($table) {
            case "User":
                require_once('../src/AppBundle/Service/UserService.php');
                $result=deleteUserDB($id);
                return $this->redirectToRoute("displayUser");
                break;
            case "Location":
                require_once('../src/AppBundle/Service/LocationService.php');
                $result=deleteLocationDB($id);
                if ($result[0]==false){
                    $this->get('session')->getFlashBag()->add(
                        'notice',
                        'There are sensors in '.$id.' location... You cant delete it'
                    );
                }
                return $this->redirectToRoute("displayLocation");
                break;
            case "Parameter":
                require_once('../src/AppBundle/Service/ParameterService.php');
                $result=deleteParameterDB($id);
                if ($result[0]==false){
                    $this->get('session')->getFlashBag()->add(
                        'notice',
                        'There are sensors in '.$id.' parameter... You cant delete it'
                    );
                }
                return $this->redirectToRoute("displayParameter");
                break;
            case "Sensor":
                require_once('../src/AppBundle/Service/SensorInfoService.php');
                $result=deleteSensorDB($id);
                if ($result[0]==false){
                    $this->get('session')->getFlashBag()->add(
                        'notice',
                        'You cant delete it'
                    );
                }
                return $this->redirectToRoute("displaySensor");
                break;
            default:
                echo "404";
                break;
        }
    }

    /**
     * @Route("/admin/search/{table}", name="searchAction")
     */
    public function search($table){
        require_once('../src/AppBundle/Service/AlarmService.php');
        $alarmCount = getAlarmCountDB();

        $word = $_POST['search_box'];
        switch ($table) {
            case "Alarm":
                require_once('../src/AppBundle/Service/AlarmService.php');
                $result = getAlarmDetailsDB("alarm_id");
                $tableData = array_slice($result,2);
                $tableDataRender = $this->searchWord($word,$tableData);
                return $this->render('adminAlarmView.html.twig',array("sortBy"=>$result[0],"tableHeader"=>$result[1],
                    "tableData"=>$tableDataRender,"alarm_count"=>$alarmCount,"tableName"=>"Alarm","url"=>"displayAlarm"));
                break;
            case "Location":
                require_once('../src/AppBundle/Service/LocationService.php');
                $result = getLocationDetailsDB("location_id");
                $tableData = array_slice($result,2);
                $tableDataRender = $this->searchWord($word,$tableData);
                return $this->render('adminTableView.html.twig',array("sortBy"=>$result[0],"tableHeader"=>$result[1],
                    "tableData"=>$tableDataRender,"alarm_count"=>$alarmCount,"tableName"=>"Location","url"=>"displayLocationSortBy"));
                break;
            case "Parameter":
                require_once('../src/AppBundle/Service/ParameterService.php');
                $result = getParameterDetailsDB("parameter_id");
                $tableData = array_slice($result,2);
                $tableDataRender = $this->searchWord($word,$tableData);
                return $this->render('adminTableView.html.twig',array("sortBy"=>$result[0],"tableHeader"=>$result[1],
                    "tableData"=>$tableDataRender,"alarm_count"=>$alarmCount,"tableName"=>"Parameter","url"=>"displayParameterSortBy"));
                break;
            case "Sensor":
                require_once('../src/AppBundle/Service/SensorInfoService.php');
                $result = getSensorDetailsDB("sensor_id");
                $tableData = array_slice($result,2);
                $tableDataRender = $this->searchWord($word,$tableData);
                return $this->render('adminTableView.html.twig',array("sortBy"=>$result[0],"tableHeader"=>$result[1],
                    "tableData"=>$tableDataRender,"alarm_count"=>$alarmCount,"tableName"=>"Sensor","url"=>"displaySensorSortBy"));
                break;
            case "User":
                require_once('../src/AppBundle/Service/UserService.php');
                $result = getUserDetailsDB("user_name");
                $tableData = array_slice($result,2);
                $tableDataRender = $this->searchWord($word,$tableData);
                return $this->render('adminTableView.html.twig',array("sortBy"=>$result[0],"tableHeader"=>$result[1],
                    "tableData"=>$tableDataRender,"alarm_count"=>$alarmCount,"tableName"=>"User","url"=>"displayUserSortBy"));
                break;
            default:
                echo "404".$table;
                break;
        }
    }

    public function searchWord($word,$tableData){
        $result = array();
        for($i = 0;$i < count($tableData);$i++){
            $isFound = false;
            for($j = 0; $j < count($tableData[$i]); $j++){
                if(strpos($tableData[$i][$j],$word) !== false){
                    $isFound = true;
                    $tableData[$i][$j] = htmlspecialchars_decode(str_replace($word,"<mark>".$word."</mark>",$tableData[$i][$j]),ENT_QUOTES);
                }
            }
            if($isFound){
                array_push($result,$tableData[$i]);
            }
        }
        return $result;
    }
}