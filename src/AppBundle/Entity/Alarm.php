<?php

namespace AppBundle\Entity;

class Alarm
{
    private $time = 'CURRENT_TIMESTAMP';
    private $alarmDescription;
    private $alarmId;
    private $sensor_id;

    public function getAlarmDescription()
    {
        return $this->alarmDescription;
    }

     function setAlarmDescription($alarmDescription)
    {
        $this->alarmDescription = $alarmDescription;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function setTime($time)
    {
        $this->time = $time;
    }

    public function getAlarmId()
    {
        return $this->alarmId;
    }

    public function setAlarmId($alarmId)
    {
        $this->alarmId = $alarmId;
    }

    public function getSensorId()
    {
        return $this->sensor_id;
    }

    public function setSensorId($sensor_id)
    {
        $this->sensor_id = $sensor_id;
    }

}