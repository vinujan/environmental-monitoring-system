<?php
/**
 * Created by PhpStorm.
 * User: senthalan
 * Date: 1/18/16
 * Time: 3:10 PM
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class DummyUser
{
    /**
     * @Assert\NotBlank()
     */
    private $userName;

    /**
     * @Assert\Length(
     *      min = 5,
     *      minMessage = "Password must have more than 5 characters",
     * )
     */
    private $newPassword;

    /**
     * @Assert\Length(
     *      min = 5,
     *      minMessage = "Password must have more than 5 characters",
     * )
     */
    private $conformPassword;

    private $roles;

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }

    /**
     * @param mixed $newPassword
     */
    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    /**
     * @return mixed
     */
    public function getConformPassword()
    {
        return $this->conformPassword;
    }

    /**
     * @param mixed $conformPassword
     */
    public function setConformPassword($conformPassword)
    {
        $this->conformPassword = $conformPassword;
    }

}