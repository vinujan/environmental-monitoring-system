<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Location
{
    /**
     * @Assert\NotBlank()
     */
    private $locationName;

    /**
     * @Assert\Range(
     *      min = 79,
     *      max = 80,
     *      minMessage = "Not a coordinate of Colombo city",
     *      maxMessage = "Not a coordinate of Colombo city"
     * )
     */
    private $longtitude;

    /**
     * @Assert\Range(
     *      min = 6,
     *      max = 7.1,
     *      minMessage = "Not a coordinate of Colombo city",
     *      maxMessage = "Not a coordinate of Colombo city"
     * )
     */
    private $latitude;

    private $locationId;


    public function getLocationName()
    {
        return $this->locationName;
    }

    public function setLocationName($locationName)
    {
        $this->locationName = $locationName;
    }

    public function getLongtitude()
    {
        return $this->longtitude;
    }

    public function setLongtitude($longtitude)
    {
        $this->longtitude = $longtitude;
    }

    public function getLocationId()
    {
        return $this->locationId;
    }

    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }
}

