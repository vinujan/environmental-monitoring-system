<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Maintenance
{

    public function getMaintenanceDate()
    {
        return $this->maintenanceDate;
    }


    public function setMaintenanceDate( $maintenanceDate)
    {
        $this->maintenanceDate = $maintenanceDate;
    }


    public function getMaintenanceDescription()
    {
        return $this->maintenanceDescription;
    }


    public function setMaintenanceDescription($maintenanceDescription)
    {
        $this->maintenanceDescription = $maintenanceDescription;
    }


    public function getMaintenanceId()
    {
        return $this->maintenance_id;
    }

    public function setMaintenanceId($maintenance_id)
    {
        $this->maintenance_id = $maintenance_id;
    }


    public function getEmployeeId()
    {
        return $this->employee_id;
    }


    public function setEmployeeId($employee_id)
    {
        $this->employee_id = $employee_id;
    }


    public function getAlarmId()
    {
        return $this->alram_id;
    }


    public function setAlarmId($alram_id)
    {
        $this->alram_id = $alram_id;
    }

    /**
     * @Assert\NotBlank()
     */
    private $maintenanceDate;

    /**
     * @Assert\NotBlank()
     */
    private $maintenanceDescription;

    private $maintenance_id;

    private $employee_id;

    private $alram_id;


    function __toString()
    {
        return sensor.employee.maintenanceId.maintenanceDescription.maintenanceDate;
    }


}

