<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Parameter
{
    public function getParameterName()
    {
        return $this->parameterName;
    }

    public function setParameterName($parameterName)
    {
        $this->parameterName = $parameterName;
    }

    public function getMaxLimit()
    {
        return $this->maxLimit;
    }

    public function setMaxLimit($maxLimit)
    {
        $this->maxLimit = $maxLimit;
    }

    public function getMinLimit()
    {
        return $this->minLimit;
    }

    public function setMinLimit($minLimit)
    {
        $this->minLimit = $minLimit;
    }

    public function getUnits()
    {
        return $this->units;
    }

    public function setUnits($units)
    {
        $this->units = $units;
    }

    public function getParameterId()
    {
        return $this->parameterId;
    }

    public function setParameterId($parameterId)
    {
        $this->parameterId = $parameterId;
    }

    /**
     * @Assert\NotBlank()
     */
    private $parameterName;

    /**
     * @Assert\NotBlank()
     */
    private $maxLimit;

    /**
     * @Assert\NotBlank()
     */
    private $minLimit;

    /**
     * @Assert\NotBlank()
     */
    private $units;

    private $parameterId;
}

