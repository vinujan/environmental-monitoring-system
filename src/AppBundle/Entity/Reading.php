<?php

namespace AppBundle\Entity;

class Reading
{
    public function getReadingValue()
    {
        return $this->readingValue;
    }
    public function setReadingValue($readingValue)
    {
        $this->readingValue = $readingValue;
    }
    public function getReadingTime()
    {
        return $this->readingTime;
    }
    public function setReadingTime($readingTime)
    {
        $this->readingTime = $readingTime;
    }
    public function getSensorId()
    {
        return $this->sensor_id;
    }
    public function setSensorId($sensor_id)
    {
        $this->sensor_id = $sensor_id;
    }

    private $readingValue;
    private $readingTime;
    private $sensor_id;
}

