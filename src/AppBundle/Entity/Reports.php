<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reports
 *
 * @ORM\Table(name="reports")
 * @ORM\Entity
 */
class Reports
{
    /**
     * @var string
     *
     * @ORM\Column(name="report_type", type="string", length=10, nullable=true)
     */
    private $reportType;

    /**
     * @var string
     *
     * @ORM\Column(name="time_period", type="string", length=20, nullable=true)
     */
    private $timePeriod;

    /**
     * @var string
     *
     * @ORM\Column(name="report_pointer", type="blob", length=16777215, nullable=true)
     */
    private $reportPointer;

    /**
     * @var string
     *
     * @ORM\Column(name="report_description", type="string", length=1000, nullable=true)
     */
    private $reportDescription;

    /**
     * @var integer
     *
     * @ORM\Column(name="report_id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $reportId;

    /**
     * @return string
     */
    public function getReportType()
    {
        return $this->reportType;
    }

    /**
     * @param string $reportType
     */
    public function setReportType($reportType)
    {
        $this->reportType = $reportType;
    }

    /**
     * @return string
     */
    public function getTimePeriod()
    {
        return $this->timePeriod;
    }

    /**
     * @param string $timePeriod
     */
    public function setTimePeriod($timePeriod)
    {
        $this->timePeriod = $timePeriod;
    }

    /**
     * @return string
     */
    public function getReportPointer()
    {
        return $this->reportPointer;
    }

    /**
     * @param string $reportPointer
     */
    public function setReportPointer($reportPointer)
    {
        $this->reportPointer = $reportPointer;
    }

    /**
     * @return string
     */
    public function getReportDescription()
    {
        return $this->reportDescription;
    }

    /**
     * @param string $reportDescription
     */
    public function setReportDescription($reportDescription)
    {
        $this->reportDescription = $reportDescription;
    }

    /**
     * @return int
     */
    public function getReportId()
    {
        return $this->reportId;
    }

    /**
     * @param int $reportId
     */
    public function setReportId($reportId)
    {
        $this->reportId = $reportId;
    }

    function __toString()
    {
        return reportType.timePeriod.reportPointer.reportDescription.reportId;
    }


}

