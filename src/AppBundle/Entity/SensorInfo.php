<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 15/12/15
 * Time: 18:46
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class SensorInfo
{
    public function getSensorType()
    {
        return $this->sensorType;
    }

    public function setSensorType($sensorType)
    {
        $this->sensorType = $sensorType;
    }

    public function getSensorAccuracy()
    {
        return $this->sensorAccuracy;
    }

    public function setSensorAccuracy($sensorAccuracy)
    {
        $this->sensorAccuracy = $sensorAccuracy;
    }

    public function getUpdateFrequency()
    {
        return $this->updateFrequency;
    }

    public function setUpdateFrequency($updateFrequency)
    {
        $this->updateFrequency = $updateFrequency;
    }

    public function getSensorIpaddress()
    {
        return $this->sensorIpaddress;
    }


    public function setSensorIpaddress($sensorIpaddress)
    {
        $this->sensorIpaddress = $sensorIpaddress;
    }


    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }


    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;
    }


    public function getSensorModel()
    {
        return $this->sensorModel;
    }


    public function setSensorModel($sensorModel)
    {
        $this->sensorModel = $sensorModel;
    }


    public function getInstalledDate()
    {
        return $this->installedDate;
    }

    public function setInstalledDate($installedDate)
    {
        $this->installedDate = $installedDate;
    }

    public function getSensorId()
    {
        return $this->sensorId;
    }

    public function setSensorId($sensorId)
    {
        $this->sensorId = $sensorId;
    }

    public function getLocationId()
    {
        return $this->locationId;
    }

    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
    }

    public function getParameterId()
    {
        return $this->parameterId;
    }

    public function setParameterId($parameterId)
    {
        $this->parameterId = $parameterId;
    }

    /**
     * @Assert\NotBlank()
     */
    private $sensorType;

    /**
     * @Assert\Range(
     *      min = 0,
     *      max = 99,
     *      minMessage = "Accuracy Percentage must be a positive value",
     *      maxMessage = "Accuracy Percentage must be a less than 100"
     * )
     */
    private $sensorAccuracy;

    /**
     * @Assert\Time()
     */
    private $updateFrequency;

    /**
     * @Assert\Ip(
     *     message = "This is not a valid IP address.",
     *     version = 4
     * )
     */
    private $sensorIpaddress;


    private $lastUpdate;

    /**
     * @Assert\NotBlank()
     */
    private $sensorModel;

    /**
     * @Assert\NotBlank()
     */
    private $installedDate;

    private $sensorId;

    /**
     * @Assert\NotBlank()
     */
    private $locationId;

    /**
     * @Assert\NotBlank()
     */
    private $parameterId;
}