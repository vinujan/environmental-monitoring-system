<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 15/12/15
 * Time: 19:34
 */
use AppBundle\Entity\Alarm;

require_once 'MysqlConnector.php';

function createAlarmDB(Alarm $alarm){
    $connection = open_database_connection();

    $query = "INSERT INTO alarm (sensor_id,alarm_description)
          VALUES (?,?);";
    $params = array($alarm->getSensorId(),$alarm->getAlarmDescription());
    mysqli_prepared_query($connection,$query,"ss",$params);

    close_database_connection($connection);
}

function getAlarmCountDB(){
    $connection = open_database_connection();
    $query = "select alarm_id from alarm where status='danger';";
    $result = mysqli_query($connection,$query);
    close_database_connection($connection);
    return mysqli_num_rows($result);
}

function updateStatus($id){
    $connection = open_database_connection();
    $query = "update alarm set status='success' where alarm_id=?;";
    mysqli_prepared_query($connection,$query,"s",array($id));
    close_database_connection($connection);
}
function getAlarmDetailsDB($sort){
    $connection = open_database_connection();
    $query = "select status,alarm_id,sensor_id,sensor_type,sensor_ipaddress,sensor_model,latitude,longtitude,time,alarm_description from sensor_info natural join alarm natural join location ORDER BY ".$sort." ASC ;";
    $result = mysqli_query($connection,$query);
    $resultArray = array();
    array_push($resultArray,array('alarm_id','sensor_id','sensor_type','status'));
    array_push($resultArray,array('alarm_id','sensor_id','sensor_type','sensor_ipaddress','sensor_model','latitude',
        'longtitude','time','alarm_description'));
    while ($row = mysqli_fetch_row($result)) {
        array_push($resultArray, $row);
    }
    close_database_connection($connection);
    return $resultArray;
}

function getAlarmSensorDB(){
    $connection = open_database_connection();
    $query = "select sensor_id from alarm where status='danger' ORDER BY sensor_id;";
    $result = mysqli_query($connection,$query);
    close_database_connection($connection);
    $resultArray = array();
    while ($row = mysqli_fetch_row($result)) {
        array_push($resultArray, $row[0]);
    }
    return array_combine($resultArray,$resultArray);
}