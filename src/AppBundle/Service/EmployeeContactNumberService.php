<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 15/12/15
 * Time: 16:38
 */


use AppBundle\Entity\EmployeeContactNumber;

require_once 'MysqlConnector.php';

function createEmployeeContactNumberDB(EmployeeContactNumber $employeeContactNumber){
    $connection = open_database_connection();

    $query = "INSERT INTO employee_contact_number (employee_id,	contact_number)
          VALUES (?,?);";
    $params = array($employeeContactNumber->getEmployee(),$employeeContactNumber->getContactNumber());
    mysqli_prepared_query($connection,$query,"ss",$params);
    close_database_connection($connection);
}