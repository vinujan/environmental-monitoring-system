<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 15/12/15
 * Time: 16:38
 */


use AppBundle\Entity\Location;

require_once 'MysqlConnector.php';

function createLocationDB(Location $location){
    $connection = open_database_connection();

    $query = "INSERT INTO location (location_name, longtitude, latitude)
          VALUES (?, ?, ?);";
    $params = array($location->getLocationName(),$location->getLongtitude(),$location->getLatitude());
    $result=mysqli_prepared_query($connection,$query,"sss",$params);
    close_database_connection($connection);
    return $result;
}

function getLastLocationId(){
    $connection = open_database_connection();

    $query = "SELECT  MAX(location_id) AS loc FROM location WHERE location_id>=?;";
    $result=mysqli_prepared_query($connection,$query,"s",array(0));
    close_database_connection($connection);

    return $result[0]['loc'];
}

function getLocationList(){
    $connection = open_database_connection();

    $query = "SELECT location_id, location_name FROM location WHERE location_id>=? ORDER BY location_name  ASC;";
    $result=mysqli_prepared_query($connection,$query,"s",array(0));
    close_database_connection($connection);

    $locList=array();
    for ($i=0; $i<count($result); $i++) {
        $locList[$result[$i]['location_name']]=$result[$i]['location_id'];
    }
    return $locList;
}

function getLocationNameList(){
    $connection = open_database_connection();

    $query = "SELECT location_id, location_name FROM location WHERE location_id>=? ORDER BY location_name  ASC;";
    $result=mysqli_prepared_query($connection,$query,"s",array(0));
    close_database_connection($connection);
    return $result;
}

function getLocationDetailsDB($sort){
    $connection = open_database_connection();
    $query = "select location_id, location_name, longtitude, latitude from location ORDER BY ".$sort." ASC ;";
    $result = mysqli_query($connection,$query);
    $resultArray = array();
    array_push($resultArray,array('location_id','location_name'));
    array_push($resultArray,array('location id','location name','longtitude','latitude'));
    while ($row = mysqli_fetch_row($result)) {
        array_push($resultArray, $row);
    }
    close_database_connection($connection);
    return $resultArray;
}

function getLocationDetailDB($id){
    $connection = open_database_connection();
    $query = "select  location_name, longtitude, latitude from location WHERE location_id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",array($id));
    close_database_connection($connection);
    return $result;
}

function deleteLocationDB($id){
    $connection = open_database_connection();
    $query = "DELETE FROM location WHERE location_id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",array($id));
    close_database_connection($connection);
    return $result;
}