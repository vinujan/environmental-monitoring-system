<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 15/12/15
 * Time: 16:38
 */


use AppBundle\Entity\Maintenance;

require_once 'MysqlConnector.php';

function createMaintenanceDB(Maintenance $maintenance){
//    $debug = $maintenance->getMaintenanceDate()->format("yyyy-mm-dd");
    $connection = open_database_connection();
    $query = "INSERT INTO maintenance (alarm_id, maintenance_date, maintenance_description, employee_id)
          VALUES (?,?,?,?);";
    $params = array($maintenance->getAlarmId(),$maintenance->getMaintenanceDate(),$maintenance->getMaintenanceDescription(),$maintenance->getEmployeeId());
    $result = mysqli_prepared_query($connection,$query,"ssss",$params);
    close_database_connection($connection);
    return $result;
}

function getMaintenanceDetail($id){
    $connection = open_database_connection();
    $query = "select alarm_id, maintenance_date, maintenance_description, employee_id from maintenance where alarm_id=?";
    $result = mysqli_prepared_query($connection,$query,"s",array($id));
    close_database_connection($connection);
    return $result;
}
