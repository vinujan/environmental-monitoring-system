<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 15/12/15
 * Time: 16:38
 */


use AppBundle\Entity\MaintenanceTeam;

require_once 'MysqlConnector.php';

function createMaintenanceTeamDB(MaintenanceTeam $maintenanceTeam){
    $connection = open_database_connection();

    $query = "INSERT INTO maintenance_team (employee_name)
          VALUES (?);";
    $params = array($maintenanceTeam->getEmployeeName());
    mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);
}

function getEmployeeListDB(){
    $connection = open_database_connection();
    $query = "select employee_id from maintenance_team ORDER BY employee_id;";
    $result = mysqli_query($connection,$query);
    close_database_connection($connection);
    $resultArray = array();
    while ($row = mysqli_fetch_row($result)) {
        array_push($resultArray, $row[0]);
    }
    return array_combine($resultArray,$resultArray);
}