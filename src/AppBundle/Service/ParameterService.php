<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 15/12/15
 * Time: 17:35
 */

use AppBundle\Entity\Parameter;
use AppBundle\Entity\Reading;

require_once 'MysqlConnector.php';

function createParameterDB(Parameter $parameter){
    $connection = open_database_connection();

    $query = "INSERT INTO parameter (parameter_name, max_limit, min_limit,units)
          VALUES (?, ?, ?, ?);";
    $params = array($parameter->getParameterName(),$parameter->getMaxLimit(),$parameter->getMinLimit(),$parameter->getUnits());
    $result=mysqli_prepared_query($connection,$query,"ssss",$params);
    close_database_connection($connection);
    return $result;
}

function getLastParameterId(){
    $connection = open_database_connection();
    $query = "SELECT  MAX(parameter_id) AS param FROM parameter WHERE parameter_id>=?;";
    $result=mysqli_prepared_query($connection,$query,"s",array(0));
    close_database_connection($connection);
    return $result[0]['param'];
}


function getParameterList(){
    $connection = open_database_connection();

    $query = "SELECT parameter_id, parameter_name FROM parameter WHERE parameter_id>=? ORDER BY parameter_name  ASC;";
    $result=mysqli_prepared_query($connection,$query,"s",array(0));
    close_database_connection($connection);

    $locList=array();
    for ($i=0; $i<count($result); $i++) {
        $locList[$result[$i]['parameter_name']]=$result[$i]['parameter_id'];
    }
    return $locList;
}

function getParameterLimit(Reading $reading){
    $connection = open_database_connection();

    $query = "SELECT min_limit, max_limit FROM parameter where parameter_id in (select parameter_id from sensor_info where sensor_info.sensor_id = ?);";
    $params = array($reading->getSensorId());
    $result=mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);

    return array($result[0]['min_limit'],$result[0]['max_limit']);
}

function getParameterDetailsDB($sort){
    $connection = open_database_connection();
    $query = "select parameter_id, parameter_name, min_limit, max_limit from parameter ORDER BY ".$sort." ASC ;";
    $result = mysqli_query($connection,$query);
    close_database_connection($connection);
    $resultArray = array();
    array_push($resultArray,array('parameter_id','parameter_name'));
    array_push($resultArray,array('parameter id','parameter name','minimum limit','maximum limit'));
    while ($row = mysqli_fetch_row($result)) {
        array_push($resultArray, $row);
    }
    return $resultArray;
}

function getParameterNameList(){
    $connection = open_database_connection();
    $query = "select parameter_id, parameter_name from parameter  ORDER BY parameter_name  ASC;";
    $result = mysqli_query($connection,$query);
    close_database_connection($connection);
    return $result;
}

function getParameterDetailDB($id){
    $connection = open_database_connection();
    $query = "select  parameter_name,units from parameter WHERE parameter_id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",array($id));
    close_database_connection($connection);
    return $result;
}

function deleteParameterDB($id){
    $connection = open_database_connection();
    $query = "DELETE FROM parameter WHERE parameter_id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",array($id));
    close_database_connection($connection);
    return $result;
}