<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 15/12/15
 * Time: 19:02
 */

use AppBundle\Entity\Reading;

require_once 'MysqlConnector.php';

function createReadingDB(Reading $reading){
    $connection = open_database_connection();

    $query = "INSERT INTO reading (sensor_id,reading_value,reading_time)
          VALUES (?,?,?);";
    $params = array($reading->getSensorId(),$reading->getReadingValue(),$reading->getReadingTime());
    mysqli_prepared_query($connection,$query,"sss",$params);

    close_database_connection($connection);
}

function checkReadingDB(Reading $reading){
    $connection = open_database_connection();
    $query = "Select insert_reading(?,?) as result;";
    $params = array($reading->getSensorId(),$reading->getReadingValue());
    $result = mysqli_prepared_query($connection,$query,"ss",$params);

    close_database_connection($connection);

    return $result[0]['result'];
}

function forDataSheet($query){
    $connection = open_database_connection();
    $result = mysqli_query($connection,$query);
    close_database_connection($connection);
    $resultArray = array();
    while ($row = mysqli_fetch_row($result)) {
        array_push($resultArray, $row);
    }
    return $resultArray;
}}

function getAvarageReadingMonthly($id,$year){
    $connection = open_database_connection();
    $query = "SELECT YEAR(`reading_time` ) AS year ,MONTH(`reading_time` ) AS month, AVG(`reading_value` ) AS avg FROM reading NATURAL JOIN sensor_info WHERE `parameter_id`=? AND YEAR(`reading_time`)=? GROUP BY YEAR( `reading_time` ) , MONTH(`reading_time` ) LIMIT 0, 12 ";
    $result = mysqli_prepared_query($connection,$query,"ss",array($id,$year));
    return $result;
}

function getAvarageReadingOnLocationMonthly($lid,$pid,$year){
    $connection = open_database_connection();
    $query = "SELECT YEAR(`reading_time` ) AS year ,MONTH(`reading_time` ) AS month, AVG(`reading_value` ) AS avg FROM reading NATURAL JOIN sensor_info WHERE `parameter_id`=? AND YEAR(`reading_time`)=? AND `location_id`=? GROUP BY YEAR( `reading_time` ) , MONTH(`reading_time` ) LIMIT 0, 12 ";
    $result = mysqli_prepared_query($connection,$query,"sss",array($pid,$year,$lid));
    close_database_connection($connection);
    return $result;
}

function getYearList(){
    $connection = open_database_connection();
    $query = "SELECT DISTINCT YEAR(`reading_time` ) AS year FROM reading";
    $result = mysqli_query($connection,$query);
    close_database_connection($connection);
    return $result;
}

