<?php
/**
 * Created by PhpStorm.
 * User: vinujan
 * Date: 15/12/15
 * Time: 17:46
 */

use AppBundle\Entity\SensorInfo;
use AppBundle\Entity\Reading;

require_once 'MysqlConnector.php';

function createSensorInfoDB(SensorInfo $sensorInfo){
    $connection = open_database_connection();

    $query = "INSERT INTO sensor_info (sensor_type, sensor_accuracy, parameter_id,update_frequency,sensor_ipaddress,last_update,sensor_model,location_id,installed_date)
          VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
    $params = array($sensorInfo->getSensorType(),$sensorInfo->getSensorAccuracy(),$sensorInfo->getParameterId()
                    ,$sensorInfo->getUpdateFrequency(),$sensorInfo->getSensorIpaddress(),$sensorInfo->getLastUpdate(),
                    $sensorInfo->getSensorModel(),$sensorInfo->getLocationId(),$sensorInfo->getInstalledDate());
    $result=mysqli_prepared_query($connection,$query,"sssssssss",$params);
    /////////////////getting lastupdate////////
    $query = "select last_update from sensor_info where sensor_id = ?";
    $params= array($sensorInfo->getSensorId());
    $result2 = mysqli_prepared_query($connection,$query,"s",$params);
    $sensorInfo->setLastUpdate($result2[0]['last_update']);
    //event creation for created sensor///////
    $query = "CREATE EVENT ".'sensor_alarm_'.$sensorInfo->getSensorId().
        " ON SCHEDULE EVERY ".$sensorInfo->getUpdateFrequency().
        " MINUTE STARTS '".$sensorInfo->getLastUpdate().
        "' DO BEGIN declare last_reading_time timestamp; declare last_timestamp timestamp default TIMESTAMPADD(MINUTE,". $sensorInfo->getUpdateFrequency().
        ",'00:01:01'); declare sensor_last_update timestamp; select reading_time into last_reading_time from reading where sensor_id =".
        $sensorInfo->getSensorId()." order by reading_time desc limit 1;if last_reading_time NOT BETWEEN last_timestamp AND CURRENT_TIMESTAMP THEN INSERT INTO alarm (sensor_id,alarm_description) VALUES (".
        $sensorInfo->getSensorId().
        ",CONCAT('Absence of reading from ',last_timestamp,' to ',CURRENT_TIMESTAMP)); end if; END";

    mysqli_query($connection,$query);
    //////////////////////////////////////////
    close_database_connection($connection);

    return $result;
}

function getLastSensorId(){
    $connection = open_database_connection();

    $query = "SELECT  MAX(sensor_id) AS num FROM sensor_info WHERE sensor_id>?;";
    $result=mysqli_prepared_query($connection,$query,"s",array(0));
    close_database_connection($connection);

    return $result[0]['num'];
}

function getSensorDetailsDB($sort){
    $connection = open_database_connection();
    $query = "select sensor_id, sensor_type, sensor_model, update_frequency, last_update, parameter_name, location_name, sensor_accuracy, installed_date, sensor_ipaddress from sensor_info natural join parameter natural join location ORDER BY ".$sort." ASC ;";
    $result = mysqli_query($connection,$query);
    $resultArray = array();
    array_push($resultArray,array("sensor_id","sensor_type","parameter_name","location_name"));
    array_push($resultArray,array('sensor id','sensor type','sensor model','update frequency','last update','parameter name','location name','sensor accuracy','installed date','sensor ipaddress'));
    while ($row = mysqli_fetch_row($result)) {
        array_push($resultArray, $row);
    }
    return $resultArray;
}

function deleteSensorDB($id){
    $connection = open_database_connection();
    $query = "DELETE FROM sensor_info WHERE sensor_id=?;";
    $result=mysqli_prepared_query($connection,$query,"s",array($id));
    close_database_connection($connection);
    return $result;
}

function updateLastUpdate(Reading $reading){
    $connection = open_database_connection();
        $query = "UPDATE sensor_info SET last_update=? WHERE sensor_id=?;";
    mysqli_prepared_query($connection,$query,"ss",array($reading->getReadingTime(),$reading->getSensorId()));
    close_database_connection($connection);
}