<?php
use AppBundle\Entity\DummyUser;
use AppBundle\Entity\User;

/**
 * Created by PhpStorm.
 * User: senthalan
 * Date: 12/15/15
 * Time: 7:53 PM
 */


require_once 'MysqlConnector.php';


function createUserDB(User $user){
    $connection = open_database_connection();

    $password=$user->getPassword();
    $encryptedpass=md5($password);

    $query = "INSERT INTO user (username,password,roles)
      VALUES (?,?,?);";
    $params = array($user->getUsername(),$encryptedpass,$user->getRole());
    $result=mysqli_prepared_query($connection,$query,"sss",$params);
    close_database_connection($connection);
    return $result;
}

//for signin


function signInUserDB(User $user){
    $connection = open_database_connection();

    $password=$user->getPassword();
    $encryptedpass=md5($password);

    $params=array( $user->getUserName());

    $query = "SELECT  interface_id, password FROM user WHERE user_name=?;";
    $result=mysqli_prepared_query($connection,$query,"s",$params);
    close_database_connection($connection);

    if (count($result)==1){
        if ($result[0]['password']==$encryptedpass){
            return $result[0]['interface_id'];
        }
        else{
            return "fail";
        }
    }
    else{
        return "fail";
    }
}

function changePasswordDB(DummyUser $user){
    $connection = open_database_connection();

    $query = "UPDATE user SET password=? WHERE username=?;";
    $params = array($user->getNewPassword(),$user->getUsername());
    mysqli_prepared_query($connection,$query,"ss",$params);
    close_database_connection($connection);
    return "password changed";

}

function getUserDetailsDB($sort){
    $connection = open_database_connection();
    $query = "select username, roles from user ORDER BY ".$sort." ASC ;";
    $result = mysqli_query($connection,$query);
    close_database_connection($connection);
    $resultArray = array();
    array_push($resultArray,array('username','roles'));
    array_push($resultArray,array('user name','Department'));
    while ($row = mysqli_fetch_row($result)) {
//        if ($row[1]=='ROLE'){
//            $row[1]="Admin";
//        }
//        else{
//            $row[1]="Maintain";
//        }

        array_push($resultArray, $row);
    }
    return $resultArray;
}

function deleteUserDB($id){
    $connection = open_database_connection();
    $query = "DELETE FROM user WHERE username=?;";
    $result=mysqli_prepared_query($connection,$query,"s",array($id));
    close_database_connection($connection);
    return $result;
}
